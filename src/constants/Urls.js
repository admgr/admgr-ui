const BASE_URL = 'http://localhost:8080';
export const ADS_URL = BASE_URL + '/api/v1/ads';
export const ADS_RANDOM_URL = BASE_URL + '/api/v1/ads/random';
export const IMAGE_NOT_FOUND_URL = 'https://via.placeholder.com/640x480?text=No+image';