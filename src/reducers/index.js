import { ADD_ADVERTISING, ADD_SEARCH_QUERY } from '../constants/ActionTypes';

const initialState = {
    ad: undefined,
    searchQuery: ''
};

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_ADVERTISING:
            return {
                ...state,
                ad: action.payload
            };
        case ADD_SEARCH_QUERY:
            return {
                ...state,
                searchQuery: action.payload
            };
        default:
            return state;
    }
}

export default rootReducer;