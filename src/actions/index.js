import { ADD_ADVERTISING, ADD_SEARCH_QUERY } from '../constants/ActionTypes';

export const addSearchQuery = payload => ({
    type: ADD_SEARCH_QUERY,
    payload
});

export const addAdvertising = payload => ({
    type: ADD_ADVERTISING,
    payload
});