import {AdSearch} from '../AdSearch';
import Button from 'react-bootstrap/Button';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import React from 'react';
import {shallow} from 'enzyme';

describe('AdSearch', function () {
    const addSearchQueryStub = jest.fn();
    const wrapper = shallow(<AdSearch addSearchQuery={addSearchQueryStub} />);
    const setStateSpy = jest.spyOn(wrapper.instance(), 'setState');

    it('should render correctly', function () {
        expect(wrapper.find(Form)).toHaveLength(1);
        expect(wrapper.find(InputGroup)).toHaveLength(1);
        expect(wrapper.find(Form.Control)).toHaveLength(1);
        expect(wrapper.find(InputGroup.Append)).toHaveLength(1);
        expect(wrapper.find(Button)).toHaveLength(1);
        expect(wrapper.find(FontAwesomeIcon)).toHaveLength(1);
    });

    it('should call addSearchQuery callback prop', function () {
        const preventDefaultStub = jest.fn();
        const searchQuery = wrapper.state('searchQuery');

        wrapper.find(Form).simulate('submit', {preventDefault: preventDefaultStub});

        expect(preventDefaultStub).toHaveBeenCalled();
        expect(addSearchQueryStub).toHaveBeenCalledWith(searchQuery);
        expect(setStateSpy).toHaveBeenCalledWith({searchQuery: ''});
    });

    it('should update state when changing form', function () {
        wrapper.find(Form.Control).simulate('change', {target: {value: 'test'}});

        expect(setStateSpy).toHaveBeenCalledWith({searchQuery: 'test'});
    });
});