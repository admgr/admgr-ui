import React from 'react';
import AdCard from '../AdCard';
import Card from 'react-bootstrap/Card';

import {shallow} from 'enzyme';

describe('AdCard', function () {
    const ad = {
        title: 'testTitle',
        description: 'testDescription',
        imageUrl: 'http://test.com/imageUrl.jpg',
        price: 5,
        credit: 25,
        dueDate: '2019-05-08'
    };
    const image = <Card.Img variant="top" src="http://test.com/imageUrl.jpg" />;
    const title = <Card.Title>testTitle</Card.Title>;
    const description = <Card.Text>testDescription</Card.Text>;
    const price = <Card.Text><small><strong>Price:</strong> $5</small></Card.Text>;
    const credit = <Card.Text><small><strong>Credit:</strong> $25</small></Card.Text>;
    const dueDate = <Card.Text><small><strong>Due Date:</strong> 2019-05-08</small></Card.Text>;

    it('should render card with full information', function () {
        const wrapper = shallow(<AdCard ad={ad} fullAd/>);

        expect(wrapper.contains(image)).toEqual(true);
        expect(wrapper.contains(title)).toEqual(true);
        expect(wrapper.contains(description)).toEqual(true);
        expect(wrapper.contains(price)).toEqual(true);
        expect(wrapper.contains(credit)).toEqual(true);
        expect(wrapper.contains(dueDate)).toEqual(true);
    });

    it('should render card with display information', function () {
        const wrapper = shallow(<AdCard ad={ad} />);

        expect(wrapper.contains(image)).toEqual(true);
        expect(wrapper.contains(title)).toEqual(true);
        expect(wrapper.contains(description)).toEqual(true);
        expect(wrapper.contains(price)).toEqual(false);
        expect(wrapper.contains(credit)).toEqual(false);
        expect(wrapper.contains(dueDate)).toEqual(false);
    });
});