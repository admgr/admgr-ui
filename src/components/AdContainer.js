import _ from 'lodash';
import AdCard from './AdCard';
import {ADS_RANDOM_URL} from '../constants/Urls';
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import {connect} from 'react-redux';
import React from 'react';
import Spinner from 'react-bootstrap/Spinner';
import PropTypes from 'prop-types';

class AdContainer extends React.Component {
    constructor() {
        super();

        this.state = {
            ad: {},
            error: false,
            errorStatus: null,
            searching: true
        }
    }

    componentDidMount() {
        this.retrieveRandomAd();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.searchQuery !== this.props.searchQuery) {
            this.retrieveRandomAd(this.props.searchQuery);
        }
    }

    render() {
        let content;
        let state = this.state;

        if (state.searching) {
            content = this.renderSearchingSpinner();
        } else if (state.error) {
            content = this.renderErrorCard(state.errorStatus);
        } else {
            content = this.renderAdCard(state.ad);
        }


        return (
            <>
                {content}
            </>
        );
    }

    renderAdCard(ad) {
        return (
            <AdCard ad={ad}/>
        )
    }

    renderErrorCard(errorStatus) {
        let message = 'Something nasty happened with the request.';

        if (errorStatus === 404) {
            message = 'No advertising found for ' + this.props.searchQuery;
        }


        return (
            <Card bg="danger" text="white">
                <Card.Body>
                    <Card.Title>Oops! Something went wrong!</Card.Title>
                    <Card.Body>{message}</Card.Body>
                </Card.Body>
            </Card>
        );
    }

    renderSearchingSpinner() {
        return (
            <div className="d-flex justify-content-center">
                <Spinner animation="grow" variant="primary"/>
            </div>
        );
    }

    retrieveRandomAd(title) {
        let url = ADS_RANDOM_URL;

        if (title) {
            url += '?title=' + title;
        }

        this.setState({
            error: false,
            errorStatus: null,
            searching: true
        });

        axios.get(url)
            .then(res => {
                console.log(res);

                this.setState({
                    ad: res.data,
                    searching: false
                });
            })
            .catch(error => {
                console.log(error);

                this.setState({
                    error: true,
                    errorStatus: _.get(error, 'response.status'),
                    searching: false
                })
            });
    }
}

AdContainer.propTypes = {
    searchQuery: PropTypes.string
};

const mapStateToProps = state => ({
    searchQuery: state.searchQuery
});

export default connect(mapStateToProps)(AdContainer);