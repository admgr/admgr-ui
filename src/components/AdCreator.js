import Button from 'react-bootstrap/Button';
import React from 'react';
import AdCreatorModal from "./AdCreatorModal";

class AdCreator extends React.Component {
    constructor() {
        super();

        this.state = {
            showModal: false
        };

        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleHideModal = this.handleHideModal.bind(this);
    }

    render() {
        return (
            <>
                <Button {...this.getButtonProps()}>Create new Ad</Button>

                <AdCreatorModal {...this.getModalProps()}/>
            </>
        );
    }

    getButtonProps() {
        return {
            block: true,
            onClick: this.handleShowModal
        };
    }

    getModalProps() {
        return {
            onHideCallback: this.handleHideModal,
            showModal: this.state.showModal
        };
    }

    handleShowModal() {
        this.setState({showModal: true});
    }


    handleHideModal() {
        this.setState({showModal: false});
    }
}

export default AdCreator;