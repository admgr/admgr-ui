import Card from 'react-bootstrap/Card';
import {IMAGE_NOT_FOUND_URL} from '../constants/Urls';
import {IMAGE_URL_REGEX} from '../constants/Regexs';
import PropTypes from 'prop-types';
import React from 'react';

class AdCard extends React.Component {
    render() {
        const ad = this.props.ad;
        const image = this.isValidImageUrl(ad.imageUrl) ? ad.imageUrl : IMAGE_NOT_FOUND_URL;

        return (
            <Card>
                <Card.Img variant="top" src={image}/>
                <Card.Body>
                    <Card.Title>{ad.title}</Card.Title>
                    <Card.Text>{ad.description}</Card.Text>
                    {this.renderExtraData(ad)}
                </Card.Body>
            </Card>
        );
    }

    renderExtraData(ad) {
        let extraData;

        if (this.props.fullAd) {
            extraData = (
                <>
                    <Card.Text>
                        <small><strong>Price:</strong> ${ad.price}</small>
                    </Card.Text>
                    <Card.Text>
                        <small><strong>Credit:</strong> ${ad.credit}</small>
                    </Card.Text>
                    <Card.Text>
                        <small><strong>Due Date:</strong> {ad.dueDate}</small>
                    </Card.Text>
                </>
            );
        }

        return extraData;
    }

    isValidImageUrl(imageUrl) {
        return imageUrl && IMAGE_URL_REGEX.test(imageUrl);
    }
}

AdCard.defaultProps = {
    fullAd: false
};

AdCard.propTypes = {
    ad: PropTypes.shape({
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        imageUrl: PropTypes.string,
        price: PropTypes.number,
        credit: PropTypes.number,
        dueDate: PropTypes.string
    }),
    fullAd: PropTypes.bool
};

export default AdCard;