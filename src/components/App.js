import AdContainer from './AdContainer';
import AdCreator from './AdCreator';
import AdSearch from './AdSearch';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import {library} from '@fortawesome/fontawesome-svg-core/index';
import {fas} from '@fortawesome/free-solid-svg-icons/index';
import Navbar from 'react-bootstrap/Navbar';
import React from 'react';
import Row from 'react-bootstrap/Row';
import './App.scss';

library.add(fas);

class App extends React.Component {
    render() {
        return (
            <div>
                <Navbar expand="md" variant="light" bg="light">
                    <Container>
                        <Navbar.Brand>Advertising and more</Navbar.Brand>
                    </Container>
                </Navbar>
                <Container className="my-3">
                    <Row className="my-3">
                        <Col className="col-md-auto">
                            <AdSearch/>
                        </Col>
                    </Row>
                    <Row className="my-3">
                        <Col md={8}>
                            <AdContainer/>
                        </Col>
                        <Col md={4}>
                            <AdCreator/>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default App;
