import {addSearchQuery} from '../actions';
import Button from 'react-bootstrap/Button';
import {connect} from 'react-redux';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome/index';
import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup';
import React from 'react';
import PropTypes from 'prop-types';

export class AdSearch extends React.Component {
    constructor() {
        super();

        this.state = {
            searchQuery: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        return (
            <Form {...this.getFormProps()}>
                <InputGroup>
                    <Form.Control {...this.getFormControlProps()} />
                    <InputGroup.Append>
                        <Button {...this.getSubmitButtonProps()}>
                            <FontAwesomeIcon icon={['fas', 'search']}/>
                        </Button>
                    </InputGroup.Append>
                </InputGroup>
            </Form>
        );
    }

    getFormProps() {
        return {
            onSubmit: this.handleSubmit
        };
    }

    getFormControlProps() {
        return {
            placeholder: 'Search Ad by title',
            onChange: this.handleChange,
            value: this.state.searchQuery
        };
    }

    getSubmitButtonProps() {
        return {
            type: 'submit'
        }
    }

    handleChange(event) {
        this.setState({searchQuery: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.addSearchQuery(this.state.searchQuery);
        this.setState({searchQuery: ''});
    }
}

AdSearch.propTypes = {
    addSearchQuery: PropTypes.func
};

const mapDispatchToProps = dispatch => ({
    addSearchQuery: query => dispatch(addSearchQuery(query))
});


export default connect(null, mapDispatchToProps)(AdSearch);