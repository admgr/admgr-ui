import AdCard from './AdCard';
import {ADS_URL} from '../constants/Urls';
import Alert from 'react-bootstrap/Alert';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Formik} from 'formik';
import {IMAGE_URL_REGEX} from '../constants/Regexs';
import InputGroup from 'react-bootstrap/InputGroup';
import Modal from 'react-bootstrap/Modal';
import PropTypes from 'prop-types';
import React from 'react';

class AdCreatorModal extends React.Component {
    constructor() {
        super();

        this.state = {
            adding: false,
            error: false,
            savedAd: null
        };

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleModalCleanupAndClose = this.handleModalCleanupAndClose.bind(this);
    }

    render() {
        let content;

        content = this.state.savedAd ? this.renderSavedAd() : this.renderForm();

        return (
            <Modal {...this.getModalProps()}>
                <Modal.Header closeButton>
                    <Modal.Title>Create new Ad</Modal.Title>
                </Modal.Header>
                {content}
            </Modal>
        )
    }

    renderForm() {
        return (
            <Formik {...this.getFormikProps()}>
                {
                    ({
                         values,
                         errors,
                         touched,
                         handleChange,
                         handleBlur,
                         handleSubmit,
                         isSubmitting
                     }) => (
                        <Form noValidate onSubmit={handleSubmit}>
                            <Modal.Body>
                                {this.renderError()}
                                <Form.Group>
                                    <Form.Label>Title</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="title"
                                        value={values.title}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isValid={touched.title && !errors.title}
                                        isInvalid={touched.title && errors.title}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {errors.title}
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control
                                        as="textarea"
                                        name="description"
                                        value={values.description}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isValid={touched.description && !errors.description}
                                        isInvalid={touched.description && errors.description}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {errors.description}
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Image Url</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="imageUrl"
                                        value={values.imageUrl}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isValid={touched.imageUrl && !errors.imageUrl}
                                        isInvalid={touched.imageUrl && errors.imageUrl}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {errors.imageUrl}
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Price</Form.Label>
                                    <InputGroup>
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>$</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control
                                            type="text"
                                            name="price"
                                            value={values.price}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            isValid={touched.price && !errors.price}
                                            isInvalid={touched.price && errors.price}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.price}
                                        </Form.Control.Feedback>
                                    </InputGroup>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Credit</Form.Label>
                                    <InputGroup>
                                        <InputGroup.Prepend>
                                            <InputGroup.Text>$</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control
                                            type="text"
                                            name="credit"
                                            value={values.credit}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            isValid={touched.credit && !errors.credit}
                                            isInvalid={touched.credit && errors.credit}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.credit}
                                        </Form.Control.Feedback>
                                    </InputGroup>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Due Date</Form.Label>
                                    <Form.Control
                                        type="date"
                                        name="dueDate"
                                        value={values.dueDate}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isValid={touched.dueDate && !errors.dueDate}
                                        isInvalid={touched.dueDate && errors.dueDate}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        {errors.dueDate}
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button {...this.getCancelButtonProps()}>Cancel</Button>
                                <Button {...this.getSubmitButtonProps(isSubmitting)}>Submit</Button>
                            </Modal.Footer>
                        </Form>
                    )
                }
            </Formik>
        );
    }

    renderError() {
        let errorAlert;

        if (this.state.error) {
            errorAlert = (
                <Alert {...this.getErrorAlertProps()}>Oops, the advertising could not be saved!</Alert>
            );
        }

        return errorAlert;
    }

    renderSavedAd() {
        return (
            <>
                <Modal.Body>
                    <Alert variant="info"><strong>Yay!</strong> A new advertising has been created!</Alert>
                    <AdCard ad={this.state.savedAd} fullAd/>
                </Modal.Body>
                <Modal.Footer>
                    <Button {...this.getCloseButtonProps()}>Close</Button>
                </Modal.Footer>
            </>
        );
    }

    getFormikProps() {
        return {
            initialValues: {
                title: '',
                description: '',
                imageUrl: '',
                price: '',
                credit: '',
                dueDate: ''
            },
            validate: this.handleFormValidation,
            onSubmit: this.handleFormSubmit
        };
    }

    getSubmitButtonProps(isSubmitting) {
        return {
            disabled: isSubmitting,
            type: 'submit'
        };
    }

    getCancelButtonProps() {
        return {
            onClick: this.props.onHideCallback,
            type: 'button',
            variant: 'outline-danger'
        };
    }

    getCloseButtonProps() {
        return {
            onClick: this.handleModalCleanupAndClose,
            type: 'button'
        };
    }

    getModalProps() {
        return {
            onHide: this.props.onHideCallback,
            show: this.props.showModal
        }
    }

    getErrorAlertProps() {
        return {
            dismissible: true,
            onClose: () => {
                this.setState({error: false});
            },
            variant: 'danger'
        };
    }

    handleFormValidation(values) {
        let errors = {};

        if (!values.title) {
            errors.title = 'Title is required';
        }

        if (!values.description) {
            errors.description = 'Description is required';
        }

        if (values.imageUrl && !IMAGE_URL_REGEX.test(values.imageUrl)) {
            errors.imageUrl = 'Enter a valid jpg/png image URL';
        }

        if (!values.price || isNaN(values.price)) {
            errors.price = 'Price is required and must be a number';
        }

        if (!values.credit || isNaN(values.credit)) {
            errors.credit = 'Credit is required and must be a number';
        }

        if (!values.dueDate) {
            errors.dueDate = 'Due date is required';
        }

        return errors;
    }

    handleFormSubmit(values, actions) {
        this.setState({adding: true});

        axios.post(ADS_URL, values)
            .then(res => {
                this.setState({
                    adding: false,
                    savedAd: res.data
                });
            })
            .catch(error => {
                this.setState({
                    adding: false,
                    error: true
                });
                actions.setSubmitting(false);
            });
    }

    handleModalCleanupAndClose() {
        this.setState({
            savedAd: null
        }, this.props.onHideCallback);
    }
}

AdCreatorModal.propTypes = {
    onHideCallback: PropTypes.func,
    showModal: PropTypes.bool
};

export default AdCreatorModal;